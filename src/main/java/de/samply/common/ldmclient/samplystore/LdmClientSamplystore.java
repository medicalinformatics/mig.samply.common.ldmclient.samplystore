/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.common.ldmclient.samplystore;

import de.samply.common.ldmclient.LdmClient;
import de.samply.common.ldmclient.LdmClientException;
import de.samply.common.ldmclient.LdmClientUtil;
import de.samply.common.ldmclient.model.QueryResultPageKey;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.query.common.Error;
import de.samply.share.model.query.common.View;
import de.samply.share.model.queryresult.common.QueryResultStatistic;
import de.samply.share.utils.QueryConverter;
import java.io.IOException;
import java.io.StringReader;
import java.util.concurrent.ExecutionException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Client to communicate with the local datamanagement implementation "Samply Store Rest"
 */
public class LdmClientSamplystore extends LdmClient<QueryResult> {

  /**
   * One or more MDR Keys were not known. List of unknown keys is attached.
   */
  public static final int ERROR_CODE_UNKNOWN_MDRKEYS = 400;
  public static final boolean CACHING_DEFAULT_VALUE = false;
  public static final int CACHE_DEFAULT_SIZE = 1000;
  static final String REST_PATH_REQUESTS = "requests";
  private static final String REST_PATH_RESULT = "result";
  private static final String REST_PATH_STATS = "stats";
  private static final String REST_PARAM_PAGE = "?page=";
  final Logger logger = LoggerFactory.getLogger(LdmClientSamplystore.class);
  private final int cacheSize;
  private boolean useCaching;


  public LdmClientSamplystore(CloseableHttpClient httpClient, String ldmBaseUrl)
      throws LdmClientException {
    this(httpClient, ldmBaseUrl, CACHING_DEFAULT_VALUE);
  }

  public LdmClientSamplystore(CloseableHttpClient httpClient, String ldmBaseUrl, boolean useCaching)
      throws LdmClientException {
    this(httpClient, ldmBaseUrl, useCaching, CACHE_DEFAULT_SIZE);
  }

  public LdmClientSamplystore(CloseableHttpClient httpClient, String ldmBaseUrl, boolean useCaching,
      int cacheSize) throws LdmClientException {
    super(httpClient, ldmBaseUrl);
    this.useCaching = useCaching;
    this.cacheSize = cacheSize;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String postView(View view) throws LdmClientSamplystoreException {

    if (view == null) {
      throw new LdmClientSamplystoreException("View is null.");
    }

    String location;
    String viewString;

    try {
      de.samply.share.model.osse.View osseView = QueryConverter.convertCommonViewToOsseView(view);
      viewString = QueryConverter
          .marshal(osseView, JAXBContext.newInstance(de.samply.share.model.osse.View.class));
    } catch (JAXBException e) {
      throw new LdmClientSamplystoreException(e);
    }

    HttpPost httpPost = new HttpPost(
        LdmClientUtil.addTrailingSlash(getLdmBaseUrl()) + REST_PATH_REQUESTS);
    HttpEntity entity = new StringEntity(viewString, Consts.UTF_8);
    httpPost.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_XML.getMimeType());
    httpPost.setHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_XML.getMimeType());
    httpPost.setEntity(entity);

    CloseableHttpResponse response;
    int statusCode;
    try {
      response = getHttpClient().execute(httpPost);
      statusCode = response.getStatusLine().getStatusCode();
      location = response.getFirstHeader(HttpHeaders.LOCATION).getValue();
      response.close();
    } catch (IOException e) {
      throw new LdmClientSamplystoreException(e);
    }

    if (statusCode != HttpStatus.SC_CREATED) {
      logger.error("Request not created. Received status code " + statusCode);
      throw new LdmClientSamplystoreException(
          "Request not created. Received status code " + statusCode);
    } else if (LdmClientUtil.isNullOrEmpty(location)) {
      throw new LdmClientSamplystoreException("Empty location received");
    }

    return location;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String postViewString(String viewString) throws LdmClientSamplystoreException {
    View view;
    try {
      view = QueryConverter.xmlToView(viewString);
    } catch (JAXBException e) {
      logger.debug(
          "JAXB Exception while trying to convert string to common view. Trying different namespace...");
      view = null;
    }

    if (view == null) {
      try {
        de.samply.share.model.osse.View osseView = QueryConverter.unmarshal(viewString,
            JAXBContext.newInstance(de.samply.share.model.osse.ObjectFactory.class),
            de.samply.share.model.osse.View.class);
        view = QueryConverter.convertOsseViewToCommonView(osseView);
      } catch (JAXBException e) {
        throw new LdmClientSamplystoreException(e);
      }
    }

    return postView(view);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public QueryResult getResult(String location) throws LdmClientSamplystoreException {
    QueryResult queryResult = new QueryResult();
    QueryResultStatistic queryResultStatistic = getQueryResultStatistic(location);

    if (queryResultStatistic != null && queryResultStatistic.getTotalSize() > 0) {
      for (int i = 0; i < queryResultStatistic.getNumberOfPages(); ++i) {
        QueryResult queryResultPage = getResultPage(location, i);
        queryResult.getEntity().addAll(queryResultPage.getEntity());
      }
      queryResult.setId(queryResultStatistic.getRequestId());
    }
    return queryResult;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public QueryResult getResultPage(String location, int page) throws LdmClientSamplystoreException {
    if (useCaching) {
      try {
        return QueryResultsCacheManager.getCache(this).get(new QueryResultPageKey(location, page));
      } catch (ExecutionException e) {
        logger.warn("Error when trying to use cache. Querying samply store directly.", e);
        return getResultPageFromSamplystore(location, page);
      }
    } else {
      return getResultPageFromSamplystore(location, page);
    }
  }

  /**
   * Get a single page of a query result from samply store
   *
   * @param location the location of the result, as retrieved from centraxx via the POST of the
   * request
   * @param page the page index
   * @return the partial query result
   */
  public QueryResult getResultPageFromSamplystore(String location, int page)
      throws LdmClientSamplystoreException, IndexOutOfBoundsException {
    if (page < 0) {
      throw new IndexOutOfBoundsException("Requested page index < 0: " + page);
    }
    Object statsOrError = getStatsOrError(location);

    if (statsOrError.getClass().equals(QueryResultStatistic.class)) {
      QueryResultStatistic queryResultStatistic = (QueryResultStatistic) statsOrError;
      if (queryResultStatistic.getNumberOfPages() < (page + 1)) { // pages start at 0
        throw new IndexOutOfBoundsException(
            "Requested page: " + page + " , number of pages is " + queryResultStatistic
                .getNumberOfPages());
      }
    } else {
      throw new LdmClientSamplystoreException("No QueryResultStatistics found at stats location.");
    }

    HttpGet httpGet = new HttpGet(
        LdmClientUtil.addTrailingSlash(location) + REST_PATH_RESULT + REST_PARAM_PAGE + page);

    try {
      CloseableHttpResponse response = getHttpClient().execute(httpGet);
      int statusCode = response.getStatusLine().getStatusCode();
      HttpEntity entity = response.getEntity();
      String queryResultString = EntityUtils.toString(entity, Consts.UTF_8);
      EntityUtils.consume(entity);
      if (HttpStatus.SC_OK == statusCode) {
        return QueryConverter.osseXmlToQueryResult(queryResultString);
      } else {
        throw new LdmClientSamplystoreException(
            "While trying to get Result page " + page + " statuscode " + statusCode
                + " was received from samply store");
      }
    } catch (IOException | JAXBException e) {
      throw new LdmClientSamplystoreException(e);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object getStatsOrError(String location) throws LdmClientSamplystoreException {
    HttpGet httpGet = new HttpGet(LdmClientUtil.addTrailingSlash(location) + REST_PATH_STATS);

    try {
      CloseableHttpResponse response = getHttpClient().execute(httpGet);
      int statusCode = response.getStatusLine().getStatusCode();
      HttpEntity entity = response.getEntity();
      String entityOutput = EntityUtils.toString(entity, Consts.UTF_8);
      if (statusCode == HttpStatus.SC_OK) {
        JAXBContext jaxbContext = JAXBContext
            .newInstance(de.samply.share.model.osse.QueryResultStatistic.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        de.samply.share.model.osse.QueryResultStatistic qrs = (de.samply.share.model.osse.QueryResultStatistic) jaxbUnmarshaller
            .unmarshal(new StringReader(entityOutput));
        EntityUtils.consume(entity);
        response.close();
        return QueryConverter.convertOsseQueryResultStatisticToCommonQueryResult(qrs);
      } else if (statusCode == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
        JAXBContext jaxbContext = JAXBContext.newInstance(de.samply.share.model.osse.Error.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        de.samply.share.model.osse.Error error = ((de.samply.share.model.osse.Error) jaxbUnmarshaller
            .unmarshal(new StringReader(entityOutput)));
        EntityUtils.consume(entity);
        response.close();
        return QueryConverter.convertOsseErrorToCommonError(error);
      } else if (statusCode == HttpStatus.SC_ACCEPTED) {
        response.close();
        logger.debug(
            "Statistics not written yet. Samply store is probably busy with another request.");
        return null;
      } else {
        response.close();
        throw new LdmClientSamplystoreException("Unexpected response code: " + statusCode);
      }
    } catch (IOException | JAXBException e) {
      throw new LdmClientSamplystoreException("While trying to read stats/error", e);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public QueryResultStatistic getQueryResultStatistic(String location)
      throws LdmClientSamplystoreException {
    Object statsOrError = getStatsOrError(location);

    if (statsOrError == null) {
      throw new LdmClientSamplystoreException("Stats not readable");
    } else if (statsOrError.getClass().equals(QueryResultStatistic.class)) {
      return (QueryResultStatistic) statsOrError;
    } else {
      throw new LdmClientSamplystoreException("Stats not available. Get error file instead.");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Error getError(String location) throws LdmClientSamplystoreException {
    Object statsOrError = getStatsOrError(location);

    if (statsOrError == null) {
      throw new LdmClientSamplystoreException("Error not readable");
    } else if (statsOrError.getClass().equals(Error.class)) {
      return (Error) statsOrError;
    } else {
      throw new LdmClientSamplystoreException(
          "Error not available. Try to get query result statistics file instead.");
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Integer getResultCount(String location) throws LdmClientSamplystoreException {
    Integer totalSize = null;
    QueryResultStatistic queryResultStatistic = getQueryResultStatistic(location);
    if (queryResultStatistic != null) {
      totalSize = queryResultStatistic.getTotalSize();
    }
    return totalSize;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getVersionString() throws LdmClientSamplystoreException {
    // TODO: Check how this is provided via samply store
    return "unknown";
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getUserAgentInfo() throws LdmClientSamplystoreException {
    // TODO: Check how this is provided via samply store
    return "samply.store/" + getVersionString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isResultPageAvailable(String location, int pageIndex) {
    if (pageIndex < 0) {
      return false;
    }

    HttpHead httpHead = new HttpHead(
        LdmClientUtil.addTrailingSlash(location) + REST_PATH_RESULT + REST_PARAM_PAGE + pageIndex);
    try {
      CloseableHttpResponse response = getHttpClient().execute(httpHead);
      int statusCode = response.getStatusLine().getStatusCode();
      return HttpStatus.SC_OK == statusCode;
    } catch (IOException e) {
      return false;
    }
  }

  public int getCacheSize() {
    return cacheSize;
  }

  public void cleanQueryResultsCache() {
    QueryResultsCacheManager.cleanCache(this);
  }
}
