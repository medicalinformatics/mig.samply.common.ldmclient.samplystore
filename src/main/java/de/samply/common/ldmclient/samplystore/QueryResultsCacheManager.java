/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.common.ldmclient.samplystore;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import de.samply.common.ldmclient.model.QueryResultPageKey;
import de.samply.share.model.osse.QueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Use caching for query results to reduce costly calls to samplystore
 */
public class QueryResultsCacheManager {

  private static Logger logger = LoggerFactory.getLogger(QueryResultsCacheManager.class);

  private static LoadingCache<QueryResultPageKey, QueryResult> queryResultCache;

  public static LoadingCache<QueryResultPageKey, QueryResult> getCache(
      final LdmClientSamplystore ldmClientSamplystore) {
    if (queryResultCache == null) {
      logger.debug("query result cache is null...creating loader");
      CacheLoader<QueryResultPageKey, QueryResult> loader = new CacheLoader<QueryResultPageKey, QueryResult>() {
        public QueryResult load(final QueryResultPageKey resultPageKey)
            throws LdmClientSamplystoreException {
          logger.debug(
              "QueryResult page was not in cache: " + resultPageKey.getLocation() + " page "
                  + resultPageKey.getPageIndex());
          return ldmClientSamplystore.getResultPageFromSamplystore(resultPageKey.getLocation(),
              resultPageKey.getPageIndex());
        }
      };
      queryResultCache = CacheBuilder.newBuilder().maximumSize(ldmClientSamplystore.getCacheSize())
          .build(loader);
    }

    return queryResultCache;
  }

  public static void cleanCache(final LdmClientSamplystore ldmClientSamplystore) {
    if (queryResultCache != null) {
      queryResultCache.invalidateAll();
      logger.debug("Cache cleaned.");
    } else {
      logger.debug("Cache was null. Nothing to do.");
    }
  }
}
