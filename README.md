# Samply Common LDM Client Samply.Store(.REST)

Samply Common LDM Client SamplyStore is a library for the communication with **L**ocal **D**ata**m**anagement systems.
It extends the abstract Samply Common LDM Client.

## Features

Provides basic operations to communicate with Samply Store Rest (may also be referenced as OSSE store rest).

## Build

In order to build this project, you need to configure maven properly and use the maven profile that
fits to your project. The Testclass points to a URL that should have an accessible samply store. Adapt
the URL before building or run with `-DskipTests`

``` 
mvn clean package
```

## Configuration

Samply Common LDM Client SamplyStore is configured during runtime by providing the necessary paramaters in the constructor.

For usage examples, see the test classes.

## Maven artifact

To use the module, include the following artifact in your project.

``` 
<dependency>
    <groupId>de.mig.samply</groupId>
    <artifactId>common-ldmclient.samplystore</artifactId>
    <version>VERSION</version>
</dependency>
``` 

